<?php

/**
 * @file
 *   Admin config settings for uc_members_only.module
 */

/**
 * Implementation of hook_menu().
 *
 * Define configuration options for Ubercart Members-only section.
 */
function uc_members_only_menu() {
  $items = array();
  $items['admin/store/settings/members_only'] = array(
    'title' => 'Members-only settings',
    'description' => 'Configure Members-only settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('uc_members_only_admin_configure'),
    'access arguments' => array('administer store'),
  //'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
 * Menu callback for admin settings.
 */
function uc_members_only_admin_configure() {
  $form = array();
  $form['uc_members_only_message_when_no_permission_to_view_sell_prices'] = array(
    '#type' => 'textfield',
    '#size' => 100,
    '#maxlength' => 200,
    '#title' => t('Text to replace the display price on the individual product page when the user does not have permission to view sell prices'),
    '#default_value' => variable_get('uc_members_only_message_when_no_permission_to_view_sell_prices', ''),
    '#description' => t('An example is: <em>"Register as a member to view prices"</em>. This text may contain tokens (e.g [model], [term], [user]) as well as HTML, including hyperlinks and style tags.')
  );
  $form['uc_members_only_alt_price_string'] = array(
    '#type' => 'textfield',
    '#size' => 100,
    '#maxlength' => 100,
    '#title' => t('If the user does not have permission to view sell or list prices, then show this text instead'),
    '#default_value' => variable_get('uc_members_only_alt_price_string', FALSE),
    '#description' => t('Examples are "suppressed", POA" and "0.00". Text may contain HTML as well as tokens, although only tokens that are not product-specific are likely to work in the contexts that this feature is used.<p>If left empty, price cells and columns are removed altogether from all pages on which they appear.')
  );
  return system_settings_form($form);
}
