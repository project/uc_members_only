
DESCRIPTION
===========
This module allows site administrators to apply more fine-grained permissions
with respect to the Ubercart product catalog and shopping cart. This is useful
in situations where you want the general public to view most of your site,
including the product catalog, but only permit certain registered members, such
as wholesalers or retailers, to see information about factory-direct prices or
to use the shopping cart.
A configurable message (may contain mark-up), eg "Sign up for an account to see
prices and buy online" is shown next to the product if the user has insufficient
permissions.

INSTALLATION
============
Installation is like any other module. Uncompress the tar-ball file, .tar.gz,
and drop it into the "sites/all/modules" subdirectory.
Visit Administer >> Site building >> Modules, tick the box in front of the
module name and press "Save configuration".

CONFIGURATION
=============
At Administer >> User management >> Permissions the site administrator will now
have the following permissions to distinguish member roles from the less
privileged, e.g. the general public, aka the anonymous role:

o 'view list prices'
o 'view sell prices'
o 'use shopping cart'

If you don't want the general public to view prices or use the shopping cart
you'd untick all three permissions for the anonymous user role and tick either
all three or 'view sell prices' and 'use shopping cart' for your member (e.g. 
wholesaler) role.
If you maintain a site that is mainly for wholesalers, but would like to
offer a taste of the what the shopping experience is like once prospective
members have signed up, then you'd typically tick the 'use shopping cart'
permission for the anonymous user role and untick the other two.
Configured like this, users can try out the shopping cart with prices removed
(or, alternatively, shown as a configurable string e.g. 0.00 -- for the latter
fill out the text field at Administer >> Store administration >> Configuration
>> Members Only settings).
With 'view sell prices' unticked, users won't be able to purchase anything as
the submit button on the final order form will be suppressed. A warning message
will appear instead.

At Store administration >> Configuration >> Members Only settings, you may enter
a message that will appear instead of the display price to users viewing the
individual product pages without the 'view sell prices' permission. This text
may contain simple HTML, for instance:

  To view factory-direct prices <a href="/user/register">click here</a> to become a member.

You may also use tokens in this text. So you could create an email link

<a href="mailto:enquiries@[site-mail]?subject=Quote for [type-name] SKU [model]: '[title]'&body=Request  from [user-name] ([user-mail]) for pricing info on [model] ([term]).">Request price</a>
This uses global, node, user and product (Ubercart) tokens.

If you do not enter anything for the message, the price display field will
simply be suppressed (or show a configurable string).

In all of the above make sure the 'view catalog' permission is checked.
Naturally sell and list prices will only displayed if so configured at 
Administer >> Store administrtion >> Configuration >> Product settings >> Product Fields
and Store administrtion >> Configuration >> Catalog >> Product Grid Settings.

USAGE
=====
Users that do not have the 'use shopping cart' will be denied access to the
following pages:
  /cart
  /cart/checkout
  /cart/checkout/review
  /cart/checkout/complete
They will also find that on any page that normally contains an 'Add to cart'
button, e.g. the individual product pages and the /products and /catalog/*
pages, this button will now be suppressed.

Similarly, for users without the 'view sell prices' permission, prices won't
appear on the individual product pages (/node/*), the catalog, the /products 
page and the shopping cart pages.

The 'view list prices' permissions applies to the individual product pages and
the /products page (if enabled on the corresponding View).

UNINSTALL
=========
Disable as per normal at the Administer >> Site building >> Modules page. Then
click the Uninstall tab.
